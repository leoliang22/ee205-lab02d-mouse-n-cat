///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02d - Mouse `n Cat - EE 205 - Spr 2022
///
/// This C++ program will either:
///   1.  Print the command line arguments in reverse order
///   2.  or, if there are no command line arguments, print all of the
///       environment variables passed into the program
///
/// @file    mouseNcat.cpp
/// @version 1.1 - Stopped printing the mouseNcat text
///
/// Compile: $ g++ -o mouseNcat mouseNcat.cpp
///
/// Usage:  mouseNcat [param1] [param2] ...
///
/// Example:
///   With a command line parameter:
///   $ ./mouseNcat I am Sam
///   Sam
///   am
///   I
///   $
///
///   Without a command line parameter:
///   $ ./mouseNcat
///   SHELL=/bin/bash
///   ...
///   SSH_TTY=/dev/pts/0
///
/// @author  Leo Liang <leoliang@hawaii.edu>
/// @date    1_February_2022
///////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <cstdlib>

// Note the new main() parameter:  envp
int main( int argc, char* argv[], char* envp[] ) {
   if (argc> 1)
   {
      for(int i=1; i< argc; i++)
      {
         std::cout << argv[argc-i] <<std::endl;
      }
   }
   else 
      {
        for(int i=1; envp[i]!= NULL; i++)
        {
          std::cout << envp[i] <<std::endl;
         }
      }
	
return 0;
std::exit( EXIT_SUCCESS );
}
